/*
 * Copyright (C) 2020  Brian Douglass
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * guess-words is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import QtQuick.Layouts 1.3
import Lomiri.Components 1.3

import "loader.js" as Loader

Page {
    id: gamePage

    property string category
    property string word: ''
    property var list
    property var resultsList
    property int timeLeft: 60
    property int countdownLeft: 3

    function pick() {
        var index = Math.floor(Math.random() * list.length);
        word = list[index];
        list = list.slice(0, index).concat(list.slice(index + 1));
    }

    Component.onCompleted: {
        resultsList = [];

        Loader.loadCategory(category, function(data) {
            list = data.words;
            header.title = data.title.toString();
            timer.start();
        });
    }

    Timer {
        id: timer
        repeat: true
        interval: 1000
        onTriggered: {
            if (countdownLeft > 0) {
                countdownLeft--;

                if (countdownLeft === 0) {
                    pick();
                }
            }
            else {
                timeLeft--;

                if (timeLeft === 0) {
                    timer.stop();
                    resultsList.push({ word: word, pass: true });

                    pageStack.pop();
                    pageStack.push(Qt.resolvedUrl('ResultsPage.qml'), {
                        resultsList: resultsList,
                    });
                }
            }
        }
    }

    header: PageHeader {
        id: header

        StyleHints {
            foregroundColor: colors.text
            backgroundColor: colors.background
            dividerColor: colors.divider
        }
    }

    Label {
        id: wordLabel
        anchors.centerIn: parent

        textSize: Label.XLarge
        text: word ? word : i18n.tr('Get ready!')
    }

    Label {
        anchors {
            horizontalCenter: wordLabel.horizontalCenter
            top: wordLabel.bottom
            topMargin: units.gu(1)
        }

        text: {
            if (countdownLeft > 0) {
                return countdownLeft;
            }

            var minutes = parseInt(timeLeft / 60);
            var seconds = timeLeft % 60;

            if (seconds < 10) {
                seconds = '0' + seconds;
            }

            return minutes + ':' + seconds;
        }
        textSize: Label.Large
    }

    Button {
        anchors {
            left: parent.left
            bottom: parent.bottom
            margins: units.gu(1)
        }

        text: i18n.tr('Pass')
        visible: countdownLeft === 0
        onClicked: {
            resultsList.push({ word: word, pass: true });
            pick()
        }
    }

    Button {
        anchors {
            right: parent.right
            bottom: parent.bottom
            margins: units.gu(1)
        }

        text: i18n.tr('Next')
        color: theme.palette.normal.positive
        visible: countdownLeft === 0
        onClicked: {
            resultsList.push({ word: word, pass: false });
            pick()
        }
    }
}
