var languages = {
    'en_US': 'English (United States)',
    'ca_ES': 'Català (Espanya)',
    'fr_FR': 'Français (France)',
    'nl_NL': 'Nederlands (Nederland)',
    'it_IT': 'Italiano (Italia)',
};
