/*
 * Copyright (C) 2020  Brian Douglass
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * guess-words is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import QtQuick.Layouts 1.3
import Lomiri.Components 1.3

Page {
    id: gamePage

    property var resultsList
    property int resultCount: 0

    Component.onCompleted: {
        for (var i = 0; i < resultsList.length; i++) {
            if (!resultsList[i].pass) {
                resultCount++;
            }
        }
    }

    header: PageHeader {
        id: header
        title: i18n.tr('Results: %1').arg(resultCount)

        StyleHints {
            foregroundColor: colors.text
            backgroundColor: colors.background
            dividerColor: colors.divider
        }
    }

    Flickable {
        anchors {
            top: header.bottom
            right: parent.right
            left: parent.left
            bottom: parent.bottom
        }
        clip: true

        ListView {
            anchors.fill: parent
            model: resultsList

            delegate: ListItem {
                height: layout.height

                ListItemLayout {
                    id: layout
                    title.text: modelData.word
                    title.color: modelData.pass ? LomiriColors.ash : theme.palette.normal.positive
                }
            }
        }
    }
}
