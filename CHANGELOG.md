# Changelog

## v1.1.5

- Updated to use Lomiri components

## v1.1.4

- Added French translations of the categories

## v1.1.3

- Added Italian translations of the categories

## v1.1.2

- Added Dutch translations of the categories

## v1.1.1

- Fixed typo

## v1.1.0

- Added option to choose a language after selecting a category
- Added Catalan translations of the categories

## v1.0.3

- New Catalan translation, thank you translators!

## v1.0.2

- New German and Italian translations, thank you translators!

## v1.0.1

- New French translation, thank you translators!

## v1.0.0

- Initial release
