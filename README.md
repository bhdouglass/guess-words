# Guess Words

Pick a category, grab a partner, and get them to guess the words on the screen before time runs out!

[![OpenStore](https://open-store.io/badges/en_US.png)](https://open-store.io/app/guess-words.bhdouglass)

## Development

Build and run using [clickable](https://github.com/bhdouglass/clickable).

## Donate

If you like this app, consider giving a small donation over at my
[Liberapay page](https://liberapay.com/bhdouglass).

## Translations

To translate Guess words into your language, create a `.po` file as usual. Then
to translate the categories/word lists create a new folder with your language
code in `qml/categories` (for example: `qml/categories/ca_ES`). Then copy the
en_US json files into that new folder. Then translate the json files. Feel free
to add local words that might not make sense to put in other languages (like
local/regional food in the "Food" category). Also, don't forget to add
the name of your translation to `qml/languages.js`.

## License

Copyright (C) 2020  Brian Douglass

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
